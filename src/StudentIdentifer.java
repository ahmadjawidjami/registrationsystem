/**
 * Created with IntelliJ IDEA.
 * User: Ahmad Jawid
 * Date: 9/6/13
 * Time: 9:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class StudentIdentifer {





    public Student createStudent(String faculty) {

        Student student = null;

        if (faculty.equalsIgnoreCase("Computer Science")) {
            student = new ComputerScienceStudent();

        } else if (faculty.equalsIgnoreCase("Engineering")) {
            student = new EngineeringStudent();

        } else if (faculty.equalsIgnoreCase("Literature")) {
            student = new LiteratureStudent();
        }


        return student;

    }

}
