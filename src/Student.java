/**
 * Created with IntelliJ IDEA.
 * User: Ahmad Jawid
 * Date: 9/6/13
 * Time: 9:22 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class Student {

    public abstract void addToDepartment();
    public abstract void createEmail();
    public abstract void createCreditSheetForm();
    public abstract void addToAttendanceSheet();




}
