/**
 * Created with IntelliJ IDEA.
 * User: Ahmad Jawid
 * Date: 9/6/13
 * Time: 10:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class StudentRegisterer {

    StudentIdentifer studentIdentifier;

    public StudentRegisterer(StudentIdentifer studentIdentifier){
        this.studentIdentifier = studentIdentifier;
    }

    public Student registerStudent(String faculty){

        Student student;
        student = studentIdentifier.createStudent(faculty);
        student.createEmail();
        student.addToDepartment();
        student.addToAttendanceSheet();
        return student;

    }

}
